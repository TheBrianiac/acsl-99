# README #

Welcome to my little repository. This project was created by Brian Schultz.

### Where's the code? ###

I, too, once asked this question. Just [click here](https://bitbucket.org/TheBrianiac/acsl-99/src/2f30188174006e8b8b502f2fd9af86298aa6ae34/src/net/brianschultz/acsl/ninetynine/?at=master) and you'll be conveniently directed to the main package.

### Usage permissions ###

All usage without written permission from the author is prohibited. The American Computer Science League and US public school systems have permission to use this for non-profit purposes. To request permission, please [email me](mailto:business@brianschultz.net).

### Problem? ###

Please [open an issue](https://bitbucket.org/TheBrianiac/acsl-99/issues/new).