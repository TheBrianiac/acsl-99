package net.brianschultz.acsl.ninetynine;

public enum CardType {

	TWO("2", 2),

	THREE("3", 3),

	FOUR("4", 4),

	FIVE("5", 5),

	SIX("6", 6),

	SEVEN("7", 7),

	EIGHT("8", 8),

	NINE("9", 9),

	TEN("T", 10),

	JACK("J", 11),

	QUEEN("Q", 12),

	KING("K", 13),

	ACE("A", 14);

	private String input;
	private int value;

	/**
	 * Construct a normal card (positive nonconditional value).
	 * 
	 * @param input the text the user enters to play this card
	 * @param value by what value this card modifies the score
	 */
	private CardType(String input, int value) {
		this.input = input;
		this.value = value;
	}

	public String getInput() {
		return input;
	}

	public int getValue() {
		return value;
	}

	public static CardType getFromInput(String input) {
		for (CardType type : CardType.values()) {
			if (type.getInput().equalsIgnoreCase(input)) {
				return type;
			}
		}
		return null;
	}

}
