package net.brianschultz.acsl.ninetynine;

public class Lang {

	/*
	 * this class just holds a bunch of string constants. it's sort of like a
	 * compiled config file. not really for config though, I just don't like to
	 * clutter other classes. "why not just inline it?" you ask? well, every
	 * time you run a line with a string anonymously defined in it, that *also*
	 * initializes a new string. call me a bytes-pincher.
	 */

	static final String EXIT_COMMAND = "exit";

	// system messages
	static final String WELCOME = "ACSL Ninety-Nine\n by Brian Schultz";
	static final String INSTRUCTIONS = "When prompted, enter a card, or \"exit\" to exit.";

	// prompts (to precede a scan#next)
	static final String STARTING_SCORE_PROMPT = "Enter starting score: ";
	static final String HAND_PROMPT = "Enter (%d) card in starting hand: ";
	static final String PLAYER_CARD_PROMPT = "Enter player's draw: ";
	static final String DEALER_CARD_PROMPT = "Enter dealer's draw: ";

	// gameplay messages & components thereof
	static final String PLAY = "%s plays %s; score %d";
	static final String WIN = "%s wins; score %d";
	static final String DEALER = "Dealer";
	static final String PLAYER = "Player";

	// error (something wrong happened)
	static final String ERROR_NUMBER = "Error: not a number. Try again.";
	static final String ERROR_CARD = "Error: no such card. Try again.";

}
