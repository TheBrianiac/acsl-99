package net.brianschultz.acsl.ninetynine;

import java.util.*;

public class NinetyNine {

	// constants (options)
	private static final int startingHandSize = 3;
	private static final boolean printDetails = true;

	// main method (when somebody runs the program)
	public static void main(String[] args) {
		// send welcome message, start main loop
		println(Lang.WELCOME);
		new NinetyNine().run();
	}

	// main routine (the main routine in a non-static setting)
	private void run() {

		// initialize a scanner (reads user input)
		Scanner scr = new Scanner(System.in);

		// send message with instructions
		println(Lang.INSTRUCTIONS);

		// repeat until told to exit
		main: while (true) {

			// ask user for a starting score.
			String startingScoreInput = scan(scr, Lang.STARTING_SCORE_PROMPT);

			// exit if the given input was an exit command
			if (isExitCommand(startingScoreInput))
				break main;

			// parse an int out of the input
			int startingScore;
			try {
				startingScore = Integer.parseInt(startingScoreInput);
			}
			catch (NumberFormatException e) {
				println(Lang.ERROR_NUMBER);
				continue main; // essentially just shows the prompt again
			}

			// success; start a game
			Game game = new Game(startingScore);
			Player player = game.getPlayer();

			// add 3 cards to player's hand
			for (int i = 1; i <= startingHandSize; i++) {
				CardType handCard = scanCardType(scr, String.format(Lang.HAND_PROMPT, i));
				if (handCard == null)
					break main;
				player.addCard(handCard);
			}

			// recyclable inits for gameplay
			CardType playerCard;
			boolean playerTurn;
			CardType playerDraw;
			boolean dealerTurn;
			CardType dealerDraw;

			// start the gameplay
			gameplay: while (true) {

				// play the player's highest card
				playerCard = player.playCard();
				playerTurn = game.play(playerCard);

				// if game ended on player turn, dealer wins
				if (playerTurn) {
					printf(Lang.WIN, Lang.DEALER, game.getScore());
					break gameplay;
				}

				// else if nobody won
				printTurnDetailsIfAsked(Lang.PLAYER, playerCard, game.getScore());

				// get player draw, add to hand
				playerDraw = scanCardType(scr, Lang.PLAYER_CARD_PROMPT);
				if (playerDraw == null) // exit command
					break main;
				player.addCard(playerDraw);

				// get dealer draw, immediately play it
				dealerDraw = scanCardType(scr, Lang.DEALER_CARD_PROMPT);
				if (dealerDraw == null) // exit command
					break main;
				dealerTurn = game.play(dealerDraw);

				// if game ended on dealer turn, player wins
				if (dealerTurn) {
					printf(Lang.WIN, Lang.PLAYER, game.getScore());
					break gameplay;
				}

				// else if nobody won
				printTurnDetailsIfAsked(Lang.DEALER, dealerDraw, game.getScore());

			} // end gameplay

		} // end main

		// close the Scanner (stop memory leaks)
		scr.close();

	} // end run

	/**
	 * Repeatedly asks user for a card type until a valid one is given.
	 * 
	 * @param scr the scanner to use
	 * @param prompt the text with which to prompt the user
	 * @return the card type, or null if exit command was given
	 */
	private CardType scanCardType(Scanner scr, String prompt) {

		// the value we'll return
		CardType cardType = null;

		// repeat until we get a valid card (or command)
		card: while (true) {

			// get input
			String input = scan(scr, prompt);

			// if it's an exit command, stop and return null
			if (isExitCommand(input))
				break card;

			// get card, check if it's real
			cardType = CardType.getFromInput(input);
			boolean validCard = !(cardType == null);

			if (validCard) {
				break card; // all good
			}
			else {
				println(Lang.ERROR_CARD);
				continue card; // try again
			}

		} // end card finder

		return cardType;
	}

	/**
	 * Prompts a user and then scans the input.
	 * 
	 * @param scr the scanner to use
	 * @param prompt the text with which to prompt the user
	 * @return the user's input
	 */
	private String scan(Scanner scr, String prompt) {
		println(prompt);
		return scr.next();
	}

	/**
	 * Check if a given String is an exit command.
	 * 
	 * @param input the given input
	 * @return true if it matches an exit command
	 */
	private static boolean isExitCommand(String input) {
		return (input.equalsIgnoreCase(Lang.EXIT_COMMAND));
	}

	/**
	 * If constant "printDetails" is true, prints the given details of a move.
	 * 
	 * @param person the person who made the move (player, dealer)
	 * @param cardType the type of card played
	 * @param score the new game score
	 */
	private void printTurnDetailsIfAsked(String person, CardType cardType, int score) {
		// optionally print result of the given move
		if (printDetails) {
			printf(Lang.PLAY, person, cardType.toString(), score);
		}
	}

	@SuppressWarnings("unused")
	// super lazy print methods
	private static void print(String s) {
		System.out.print(s);
	}

	private static void printf(String s, Object... args) {
		System.out.printf(s + "\n", args);
	}

	private static void println(String s) {
		System.out.println(s);
	}

}
