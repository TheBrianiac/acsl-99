package net.brianschultz.acsl.ninetynine;

import java.util.*;

public class Player {

	private ArrayList<CardType> hand = new ArrayList<>();

	/**
	 * Adds a card to the player's hand.
	 * 
	 * @param cardType the type of the card
	 */
	public void addCard(CardType cardType) {
		hand.add(cardType);
	}

	/**
	 * Finds the card with the highest face value, removes it, and returns it.
	 * 
	 * @return the card's type
	 */
	public CardType playCard() {

		// find the card with the highest face value
		CardType highest = hand.get(0);
		for (CardType cardType : hand) {
			if (cardType.getValue() > highest.getValue()) {
				highest = cardType;
			}
		}

		// remove it (playing it = can't play it again)
		// TODO maybe, for principle, don't go through the whole list twice
		hand.remove(highest);

		// return the value (this part actually plays it)
		return highest;
	}

}
