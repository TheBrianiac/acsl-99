package net.brianschultz.acsl.ninetynine;

public class Game {

	// fields
	private int score;
	private Player player;

	public Game(int startingScore) {
		score = startingScore;
		player = new Player();
	}

	/**
	 * Plays a card into the game.
	 * 
	 * @param cardType the type of the card
	 * @return true if game is over
	 */
	public boolean play(CardType cardType) {

		// do operations according to the card type
		switch (cardType) {
			// special cards
			case ACE: // ace adds 14, unless that puts it over 99, in which case
						// it adds 1
				int fourteen = score + 14;
				if (fourteen > 99) {
					score += 1;
				}
				else {
					score = fourteen;
				}
				break;
			case NINE: // nine is a pass, it does nothing
				break;
			case TEN: // ten subtracts 10
				score -= 10;
				break;
			// normal cards (worth face value)
			default:
				score += cardType.getValue();
				break;
		}

		// if the score is under 100, game continues
		if (score <= 99) {
			return false;
		}
		// otherwise, somebody won
		else {
			return true;
		}
	}

	// field getters
	public int getScore() {
		return score;
	}

	public Player getPlayer() {
		return player;
	}

}
